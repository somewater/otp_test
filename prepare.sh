#!/usr/bin/env bash
cd "$(dirname "$0")"
TARGET_JAR=otp_test-1.1.0-SNAPSHOT-shaded.jar

# building project
if [ ! -f "target/$TARGET_JAR" ]; then
    echo "Building project..."
    mvn clean package
fi

# download OTP data
if [ ! -d otp ]; then
    if [ ! -f otp.zip ]; then
       echo "Load OTP data..."
       wget "https://dl.dropboxusercontent.com/u/69570/beam/otp.zip"
       echo "Data loaded successfully"
    fi

    unzip otp.zip

    # regenerate 'Graph.obj' because original file from archive consist of classes with serialVersionUID=0
    # (see https://gist.github.com/Somewater/427e0affeb19881370fa49cd62da6af7)
    mv otp/graphs/sf/Graph.obj otp/graphs/sf/Graph.obj.bak
    echo "Regenerate Graph.obj..."
    java -Xmx2G -cp target/$TARGET_JAR org.opentripplanner.standalone.OTPMain --build otp/graphs/sf
fi

# rebuild Graph.obj if it was deleteed
if [ ! -f otp/graphs/sf/Graph.obj ]; then
    echo "Rebuild Graph.obj..."
    java -Xmx2G -cp target/$TARGET_JAR org.opentripplanner.standalone.OTPMain --build otp/graphs/sf
fi

java -Xmx2G -cp target/$TARGET_JAR planner.Main --basePath otp
