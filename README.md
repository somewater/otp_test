# Project description

### Application start
* Invoke `./prepare.sh` script. It will load all depenencies
(include road network data), build project and run it.
* Every press of `Enter` creates a "client": actor that sends request
and prints response or prints error if it doesn't receive response
within a minute.
* Run tests: `mvn test`

### Overview
Application consists of several parts:

* `TripPlanner`, `TripPlannerImpl` - wrapper around OTP library.
Every instance of `TripPlanner` initializes one separated
instance of OTP GraphService.
* `Main` class is an application entry point. It starts several `Worker`
actors (according with "--workers [num]" command line param or
number of CPU cores) and passes personal `TripPlanner` for everyone.
* Actually, to work on one machine, it is enough to start one worker
and access it in parallel. It's because OTP supports parallel
processing of requests. However, it is possible to distribute
the workers to different servers. In this case, the `Distributor`
will evenly load them.
* After receiving the request, `Distributor` checks the availability
of initialized workers (whose `TripPlanner` service has already loaded).
If there are such, it chooses the least loaded. If there is
no initialized workers or each worker fully loaded now,
`Distributor` responds with an error status.
