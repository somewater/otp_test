package planner

import org.opentripplanner.api.model.TripPlan
import org.opentripplanner.routing.core.RoutingRequest

import scala.util.Try

/**
  * Message protocol for service communication
  */
object Protocol {
  case class Request(request: RoutingRequest)
  case class Response(response: Try[TripPlan])
}
