package planner.services

import java.io.File
import java.util.concurrent.atomic.{AtomicBoolean, AtomicReference}

import org.opentripplanner.api.model.TripPlan
import org.opentripplanner.api.resource.GraphPathToTripPlanConverter
import org.opentripplanner.routing.core.RoutingRequest
import org.opentripplanner.routing.graph.Graph.LoadLevel
import org.opentripplanner.routing.impl.{GraphPathFinder, InputStreamGraphSource}
import org.opentripplanner.routing.services.GraphService
import org.opentripplanner.standalone.CommandLineParameters
import TripPlannerImpl._

import scala.concurrent.{ExecutionContext, Future}

/**
  * @param params
  * @param ec - execution context for heavy blocking operations. Do not share it with akka ex context!
  */
class TripPlannerImpl(params: CommandLineParameters)(implicit ec: ExecutionContext) extends TripPlanner {

  private val graphService = new AtomicReference[GraphService]

  override def initialize(): Future[Unit] = Future {
    val routes = findGraphFiles(params.graphDirectory)
    val graphService = buildGraphService(params)
    routes.foreach {
      case RouteData(routerId, graphDir) =>
        val graphSource = InputStreamGraphSource.newFileGraphSource(routerId, graphDir, LoadLevel.FULL)
        graphService.registerGraph(routerId, graphSource)
    }
    routes.headOption.foreach(p => graphService.setDefaultRouterId(p.name))
    this.graphService.set(graphService)
  }

  override def query(request: RoutingRequest): Future[TripPlan] = {
    val service = graphService.get()
    if (service eq null) {
      Future.failed(new RuntimeException("Not initialized yet"))
    } else Future {
      val router = service.getRouter(request.routerId)
      val gpFinder = new GraphPathFinder(router)
      val paths = gpFinder.graphPathFinderEntryPoint(request)
      GraphPathToTripPlanConverter.generatePlan(paths, request)
    }
  }

  private def findGraphFiles(graphDirectory: File): Seq[RouteData] = {
    graphDirectory.list().flatMap { sub =>
      val subPath = new File(graphDirectory, sub)
      if (subPath.isDirectory) {
        if (subPath.exists() && subPath.canRead) {
          RouteData(sub, subPath) :: Nil
        } else {
          Nil
        }
      } else {
        Nil
      }
    }
  }

  private def buildGraphService(params: CommandLineParameters): GraphService = {
    val graphService = new GraphService(true)
    val graphSourceFactory = new InputStreamGraphSource.FileFactory(params.graphDirectory)
    graphService.graphSourceFactory = graphSourceFactory
    graphSourceFactory.basePath = params.graphDirectory
    graphService
  }
}

private object TripPlannerImpl {
  private case class RouteData(name: String, graphDir: File)
}
