package planner.services

import org.opentripplanner.api.model.TripPlan
import org.opentripplanner.routing.core.RoutingRequest

import scala.concurrent.Future

/**
  * Interface fro OTP library communication
  */
trait TripPlanner {

  def initialize(): Future[Unit]

  def query(request: RoutingRequest): Future[TripPlan]
}
