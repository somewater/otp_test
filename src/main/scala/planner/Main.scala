package planner

import java.util.concurrent.Executors

import akka.actor.ActorSystem
import com.beust.jcommander.JCommander
import org.opentripplanner.standalone.CommandLineParameters
import org.slf4j.LoggerFactory
import planner.actors.{ClientActor, DistrubutorActor, WorkerActor}
import planner.services.TripPlannerImpl

import scala.concurrent.ExecutionContext
import scala.io.StdIn

/**
  * Application entry point
  * Provided arguments:
  *   --basePath "path/to/otp" - required
  *   (another OpenTripPlanner arguments)
  *
  *   --workers [int]
  */
object Main {
  def main(args: Array[String]): Unit = {
    val log = LoggerFactory.getLogger(getClass)
    val params = readProgramArgs(args)
    val concurrency = Runtime.getRuntime.availableProcessors()
    val numWorkers = args.sliding(2, 1).
      toList.
      collectFirst { case Array("--workers", num) => num.toInt }.
      getOrElse(2)
    val maxParallelismPerWorker = math.max(2, (concurrency.toDouble / numWorkers).toInt)

    val blockedPool = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(concurrency))

    implicit val actorSystem = ActorSystem("TripPlanner")
    val workers =
      (0 until numWorkers).map { index =>
        val service = new TripPlannerImpl(params)(blockedPool)
        actorSystem.actorOf(WorkerActor.props(service), s"Worker_$index")
      }.toList

    val distributor = actorSystem.actorOf(DistrubutorActor.props(workers, maxParallelismPerWorker), "Distributor")

    var clientIndex = 0
    while (true) {
      actorSystem.actorOf(ClientActor.props(distributor), s"Client_$clientIndex")
      log.info(s"Client #$clientIndex created")
      clientIndex += 1
      StdIn.readLine()
    }
  }

  def readProgramArgs(args: Array[String]): CommandLineParameters = {
    val params = new CommandLineParameters()
    val jc = new JCommander()
    jc.setAcceptUnknownOptions(true)
    jc.addObject(params)
    jc.parse(args: _*)
    params.infer()
    params
  }
}


