package planner

import java.text.SimpleDateFormat
import java.util.Collections

import org.opentripplanner.api.parameter.QualifiedModeSet
import org.opentripplanner.common.model.GenericLocation
import org.opentripplanner.routing.core.RoutingRequest
import org.opentripplanner.util.ResourceBundleSingleton

import scala.util.Random

/**
  * Generates random routing requests
  */
object RandomGen {
  private val rnd = new Random(123)
  private val yyyyMMdd = new ThreadLocal[SimpleDateFormat]() {
    override def initialValue(): SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd")
  }

  def next() = {
    val request = new RoutingRequest()
    // San Francisco area:
    request.from = new GenericLocation(random(37.778448826783546, 37.784282779035216),
                                       random(-122.4930953979492, -122.21466064453125))
    // Oakland area:
    request.to = new GenericLocation(random(37.77722770873696, 37.802256596712596),
                                     random(-122.18968391418457, -122.2361183166504))
    request.dateTime = (random(yyyyMMdd.get().parse("2016-01-01").getTime,
      yyyyMMdd.get().parse("2016-12-31").getTime) / 1000).toLong

    // Default settings
    request.walkSpeed = 2.0
    request.stairsReluctance = 4.0
    request.carDropoffTime = 240
    request.setWheelchairAccessible(false)
    request.setArriveBy(false)
    request.setIntermediatePlacesFromStrings(Collections.emptyList())
    new QualifiedModeSet("TRANSIT,WALK").applyToRoutingRequest(request)
    request.setModes(request.modes)
    request.locale = ResourceBundleSingleton.INSTANCE.getLocale("en")
    request.setMaxWalkDistance(100000000.0)
    request.maxTransferWalkDistance = 100000000.0
    request
  }

  @inline
  private def random[T](from: T, to: T)(implicit numeric: Numeric[T]): Double = {
    import numeric._
    if (from < to) from.toDouble() + (to - from).toDouble() * rnd.nextDouble()
    else to.toDouble() + (from - to).toDouble() * rnd.nextDouble()
  }
}
