package planner.actors

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Stash}
import planner.services.TripPlanner

import scala.util.{Failure, Success, Try}
import WorkerActor._
import akka.pattern.pipe

import scala.util.control.NonFatal
import planner.Protocol

/**
  * In fact, you can achieve parallelism by creating one TripPlanner service and Worker and sending
  * to it tasks in parallel
  */
class WorkerActor(tripPlannerService: TripPlanner) extends Actor with ActorLogging with Stash {
  import context.dispatcher

  def receive = notInitialized

  val watchers = collection.mutable.ListBuffer.empty[ActorRef]

  override def preStart(): Unit = {
    log.info("Graph loading started...")
    tripPlannerService.initialize().map(v => Initialized) pipeTo self
  }

  def notInitialized: Receive = {
    case request: Protocol.Request =>
      log.info("Request stashed")
      stash()
    case Initialized =>
      log.warning("Service initialized")
      unstashAll()
      watchers.foreach(_ ! Inited(worker = self))
      watchers.clear()
      context.become(initialized)
    case Watch(actor) =>
      watchers += actor
  }

  def initialized: Receive = {
    case Protocol.Request(routingRequest) =>
      log.info("Request handling started")
      tripPlannerService.query(routingRequest).
        map { plan => Protocol.Response(Success(plan)) }.
        recover { case NonFatal(ex) => Protocol.Response(Failure(ex)) }.
        pipeTo(sender)
    case Watch(actor) =>
      actor ! Inited(worker = self)
  }
}

object WorkerActor {
  // Protocol of Worker watching
  case class Watch(actor: ActorRef)
  case class Inited(worker: ActorRef)

  def props(tripPlannerService: TripPlanner) = Props(new WorkerActor(tripPlannerService))

  private object Initialized
}
