package planner.actors

import akka.actor.{Actor, ActorLogging, ActorRef, Cancellable, Props}

import scala.concurrent.duration._
import ClientActor._
import akka.actor.Actor.Receive
import planner.{Protocol, RandomGen}

import scala.util.{Failure, Success}

/**
  * It's client simulator: after creation it sends routing request to the distributor and waits result.
  * If nothing comes up withing a minute, it prints error and stops
  */
class ClientActor(distributor: ActorRef) extends Actor with ActorLogging {

  import context.dispatcher
  var timeoutSchedule: Option[Cancellable] = None

  override def preStart(): Unit = {
    timeoutSchedule = Some {
      context.system.scheduler.scheduleOnce(timeout, self, Timeout)
    }
    distributor ! Protocol.Request(RandomGen.next())
  }

  override def receive: Receive = {
    case Protocol.Response(Success(plan)) =>
      log.info("Request handled with success, itineraries: {}", plan.itinerary.size())
      stop()
    case Protocol.Response(Failure(ex)) =>
      log.info("Request handled with failure: {}", ex)
      stop()
    case Timeout =>
      log.error("Service not responds within {}", timeout)
      stop()
  }

  private def stop() = {
    timeoutSchedule.foreach(_.cancel())
    context.stop(self)
  }
}

object ClientActor {
  val timeout = 1 minute
  private object Timeout

  def props(distributer: ActorRef) = Props(new ClientActor(distributer))
}
