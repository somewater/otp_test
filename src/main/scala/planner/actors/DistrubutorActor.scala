package planner.actors

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Terminated}
import planner.Protocol
import akka.pattern.ask
import akka.pattern.pipe
import planner.actors.DistrubutorActor.Result

import scala.util.Failure

/**
  * Distributes work between workers and controls maximum load
  */
class DistrubutorActor(workers: Seq[ActorRef], maxParallelismPerWorker: Int) extends Actor with ActorLogging {

  import context.dispatcher

  // worker => tasks in process
  val availableWorkers = collection.mutable.HashMap.empty[ActorRef, Int].withDefaultValue(0)

  override def preStart(): Unit = {
    workers.foreach { worker =>
      worker ! WorkerActor.Watch(self)
      context.watch(worker)
    }
  }

  def receive = {
    case WorkerActor.Inited(worker) =>
      availableWorkers.put(worker, 0)
      log.warning("Worker inited, awailable workers: {}", availableWorkers.size)

    case Terminated(worker) =>
      availableWorkers.remove(worker)
      log.warning("Worker crached, awailable workers: {}", availableWorkers.size)

    case request: Protocol.Request =>
      if (availableWorkers.isEmpty) {
        sender() ! Protocol.Response(Failure(new RuntimeException("OTP graph loading not completed, try later")))
      } else {
        pullAvailableWorker() match {
          case Some(worker) =>
            val initialSender = sender()
            worker.ask(request)(timeout = ClientActor.timeout, sender = self).
              recover { case ex => Protocol.Response(Failure(ex)) }.
              mapTo[Protocol.Response].
              map(response => Result(response, worker, initialSender)).
              pipeTo(self)
          case None =>
            sender() ! Protocol.Response(Failure(new RuntimeException("Service overloaded, try later")))
        }
      }
    case Result(response, worker, client) =>
      availableWorkers.put(worker, availableWorkers(worker) - 1)
      client ! response
  }

  def pullAvailableWorker(): Option[ActorRef] = {
    if (availableWorkers.isEmpty) {
      None
    } else {
      availableWorkers.minBy(_._2) match {
        case (worker, tasks) =>
          if (tasks < maxParallelismPerWorker) {
            availableWorkers.put(worker, tasks + 1)
            Some(worker)
          } else {
            None
          }
      }
    }
  }
}

object DistrubutorActor {
  private case class Result(response: Protocol.Response, worker: ActorRef, client: ActorRef)

  def props(workers: Seq[ActorRef], maxParallelismPerWorker: Int) =
    Props(new DistrubutorActor(workers, maxParallelismPerWorker))
}