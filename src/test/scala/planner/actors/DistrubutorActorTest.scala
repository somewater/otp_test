package planner.actors

import akka.actor.ActorSystem
import akka.testkit.{TestKit, TestProbe}
import org.scalatest.{Matchers, WordSpecLike}
import planner.{Protocol, RandomGen}

import scala.util.Failure
import scala.concurrent.duration._

class DistrubutorActorTest extends TestKit(ActorSystem("Test")) with WordSpecLike with Matchers {

  abstract class Fixture {
    implicit def self = testActor
    lazy val maxParallelismPerWorker = 2
    lazy val workerActors = Seq(TestProbe("Worker_1"), TestProbe("Worker_2"), TestProbe("Worker_3"))
    private def workers = workerActors.map(_.ref)
    val distributor = system.actorOf(DistrubutorActor.props(workers, maxParallelismPerWorker))
    val timeout = 200 milliseconds // for faster test
  }

  "DistributedActor" should {
    "respond with error in case of abcence initialized workers" in new Fixture {
      distributor ! Protocol.Request(RandomGen.next())
      expectMsgPF() {
        case Protocol.Response(Failure(ex)) =>
          ex.getMessage should include ("OTP graph loading not completed")
      }
    }

    "respond with error if every worker fully loaded" in new Fixture {
      assert(workerActors.nonEmpty, "By test design")

      info("Initialize all workers")
      workerActors.foreach { worker =>
        worker.expectMsgClass(classOf[WorkerActor.Watch]) // distributer starts with this message
        worker.expectNoMsg(timeout)
        distributor ! WorkerActor.Inited(worker.ref)
      }

      (0 until (workerActors.size * maxParallelismPerWorker)).foreach {
        idx =>
          distributor ! Protocol.Request(RandomGen.next())
      }
      workerActors.foreach { worker =>
        (0 until maxParallelismPerWorker).foreach { i =>
          worker.expectMsgClass(classOf[Protocol.Request])
        }
        worker.expectNoMsg(timeout)
      }

      distributor ! Protocol.Request(RandomGen.next())
      expectMsgPF() {
        case Protocol.Response(Failure(ex)) =>
          ex.getMessage should include ("Service overloaded")
      }
    }

    "select less loaded worker" in new Fixture {
      info("Initialize all workers")
      workerActors.foreach { worker =>
        worker.expectMsgClass(classOf[WorkerActor.Watch])
        worker.expectNoMsg(timeout)
        distributor ! WorkerActor.Inited(worker.ref)
      }

      info("First worker should receive message")
      distributor ! Protocol.Request(RandomGen.next())
      awaitCond(workerActors.exists(_.msgAvailable))
      workerActors.filter(_.msgAvailable) should have size(1)
      val firstWorker = workerActors.filter(_.msgAvailable).head
      firstWorker.expectMsgClass(classOf[Protocol.Request])

      info("Middle worker should receive message")
      distributor ! Protocol.Request(RandomGen.next())
      awaitCond(workerActors.exists(_.msgAvailable))
      workerActors.filter(_.msgAvailable) should have size(1)
      val middleWorker = workerActors.filter(_.msgAvailable).head
      middleWorker.expectMsgClass(classOf[Protocol.Request])
      firstWorker shouldNot equal(middleWorker)

      info("Last worker should receive message")
      distributor ! Protocol.Request(RandomGen.next())
      awaitCond(workerActors.exists(_.msgAvailable))
      workerActors.filter(_.msgAvailable) should have size(1)
      val lastWorker = workerActors.filter(_.msgAvailable).head
      lastWorker.expectMsgClass(classOf[Protocol.Request])
      firstWorker shouldNot equal(lastWorker)
      middleWorker shouldNot equal(lastWorker)

      info("Reply from middle worker")
      middleWorker.reply(Protocol.Response(Failure(new RuntimeException("Test exception 1"))))
      expectMsgPF() {
        case Protocol.Response(Failure(ex)) =>
          ex.getMessage should include ("Test exception 1")
      }

      info("Should send next task to the middle worker")
      distributor ! Protocol.Request(RandomGen.next())
      middleWorker.expectMsgClass(classOf[Protocol.Request])
      workerActors.foreach(_.expectNoMsg(timeout))
    }

    "correctly transfer response from worker" in new Fixture {
      info("Initialize all workers")
      workerActors.foreach { worker =>
        worker.expectMsgClass(classOf[WorkerActor.Watch])
        worker.expectNoMsg(timeout)
        distributor ! WorkerActor.Inited(worker.ref)
      }

      val client1 = TestProbe("Client_1")
      val client2 = TestProbe("Client_2")
      val request1 = RandomGen.next()
      val request2 = RandomGen.next()
      request1.routerId = "Route 1"
      request2.routerId = "Route 2"

      distributor.tell(Protocol.Request(request1), client1.ref)
      distributor.tell(Protocol.Request(request2), client2.ref)

      info("Wait until both worker received request")
      awaitCond(workerActors.count(_.msgAvailable) == 2)
      workerActors.filter(_.msgAvailable).toList.foreach {
        worker =>
          val msg = worker.expectMsgClass(classOf[Protocol.Request])
          worker.reply(Protocol.Response(Failure(new RuntimeException(s"Test exception from ${msg.request.routerId}"))))
      }

      info("Should transfer response #1 to the client #1")
      client1.expectMsgPF(max = timeout) {
        case Protocol.Response(Failure(ex)) =>
          ex.getMessage should include ("Test exception from Route 1")
      }

      info("Should transfer response #2 to the client #2")
      client2.expectMsgPF(max = timeout) {
        case Protocol.Response(Failure(ex)) =>
          ex.getMessage should include ("Test exception from Route 2")
      }
    }
  }
}
