import java.io.*;
 
/**
 * Read binary dump of serialized objects and print serialVersionUID of classes.
 * Usage: java PrintUIDs path/to/dump
 *
 * Based on http://stackoverflow.com/questions/1321988/finding-serialversionuid-of-serialized-object#1322186
 */
public class PrintUIDs extends ObjectInputStream {
 
    public PrintUIDs(InputStream in) throws IOException {
        super(in);
    }
 
    @Override
    protected ObjectStreamClass readClassDescriptor() throws IOException,
            ClassNotFoundException {
        ObjectStreamClass descriptor = super.readClassDescriptor();
        System.out.print("name=" + descriptor.getName());
        System.out.println(", serialVersionUID=" + descriptor.getSerialVersionUID());
        return descriptor;
    }
 
    public static void main(String[] args) throws IOException {
        InputStream in = new FileInputStream(args[0]);
        ObjectInputStream ois = new PrintUIDs(in);
        try {
            ois.readObject();
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
    }
}

